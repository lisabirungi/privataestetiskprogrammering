//defination globale værdier og variabler
var flowerNiveau;
var flowerNiveau = [];
var button;

let indhold;
let input;
let minTid = 0;
let i = 2;


function preload() {

  //preload af billeder samt font
  imgB = loadImage("baggrund2.png");
  imgH = loadImage("HANAOTCHI.png");
  font = loadFont("Retro_Gaming.ttf");

  //oprettesle af array
  flowerNiveau = [];

  var imgCount = 5; //antal af billeder
  //loop der loader billederne ind i et array
  for (var i = 0; i < 5; i++) {
    flowerNiveau[i] = loadImage(i + ".jpg");
  }
}


function setup() {

  createCanvas(windowWidth, windowHeight);

  //tjek om flowerNiveau fungere via console log
  console.log(flowerNiveau);

  //input dannelse og styling
  input = createInput();
  input.position(685, 500);
  input.addClass("inputStyling");

  //button dannelse og styling
  button = createButton("Giv mig data!");
  button.position(1130, 493);
  button.mouseClicked(newNiveau);
  button.addClass("buttonStyling");

}


function newNiveau() {

  //if statement for hvis man indtaster data inden 20 sekunder samt clearing af input efter
  indhold = input.value();

  if (indhold != '' && i < 4) {
    i++
    input.value('');
  }

  //tjek om indhold fungere via console log
  console.log(indhold);

}

function draw() {
  //visning af baggrund og hanaotchi console
  background(imgB);
  image(imgH, 100, 100);

  //infoboks styling
  fill(255, 255, 255, 150);
  strokeWeight(3);
  stroke(255, 197, 213);
  rect(650, 100, 700, 500, 20);

  //tekststyling
  textFont(font);
  textSize(20);
  fill(0);
  stroke(255, 197, 213);
  text("HANAOTCHI", 685, 155);
  textSize(15);
  text("Velkommen! Jeg er din HANAOTCHI!", 685, 200);
  text("Jeg er din nye data-blomst og mit liv afhænger af du konstant", 685, 230);
  text("giver mig din data. Jeg er meget sart og skal have data inden", 685, 260);
  text("der er gået 20 sekunder. Hvis du ikke gør det dræber du mig tihi.", 685, 290);
  text("Skriv din data i tekstboksen og tryk på knappen for at fodre mig.", 685, 335);
  text("Gør du det inden for 20 sekunder blomstre jeg. Hvis du ikke gør det", 685, 365);
  text("det visner jeg og dør. Glæder mig til at slubre din data i mig. Mit ", 685, 395);
  text("liv er i dine hænder.", 685, 425);


  //if statement for hvis man ikke indtaster data inden 20 sekunder
  minTid++

  if (minTid > 1200 && i > 0) {
    i--
    minTid = 0;
  }

  //visning af datablomsten
  image(flowerNiveau[i], 200, 240, 205, 205)

  //tjek om minTid fungere via console log
  // console.log(minTid);
}





//REFERENCELISTE

//FONT
//https://www.dafont.com/retro-gaming.font

//BILLEDER 
//Blomst 1 - https://www.freepik.com/free-vector/flat-design-flower-pixel-art-illustration_22631535.htm#&position=7&from_view=undefined
//Blomst 2 - https://www.freepik.com/free-vector/flat-design-flower-pixel-art-illustration_22631540.htm#&position=5&from_view=undefined
//Blomst 3 - https://www.freepik.com/free-vector/flat-design-flower-pixel-art-illustration_22631537.htm#&position=3&from_view=undefined
//Blomst 4 - https://www.freepik.com/free-vector/flat-design-flower-pixel-art-illustration_22631541.htm#&position=1&from_view=undefined

//p5.js
//input - https://p5js.org/reference/#/p5/input
//mousePressed - https://p5js.org/reference/#/p5.Element/mousePressed
