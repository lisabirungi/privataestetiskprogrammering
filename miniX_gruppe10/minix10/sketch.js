//definering af globale værdier
let sentiment;
let statusEl;
let submitBtn;
let inputBox;
let sentimentResult;

function setup() {
  //noCanvas fjerner default canvas for sketch
  noCanvas();
  
  //initialisere sentiment
  sentiment = ml5.sentiment('movieReviews', modelReady);

  //---opsætning af html siden/miljøet---
  statusEl = createP('Loading Model...'); //Status for loading
  inputBox = createInput('Write a comment....'); //Inputbox hvor man kan indtaste sætning
  inputBox.attribute('size', '40'); //styling af tekstbox
  submitBtn = createButton('Comment'); //knap hvor man submitter sin sætning
  sentimentResult = createP('Sentiment score:'); // output hvor sentiment score vises 

  // predicting the sentiment on mousePressed()
  submitBtn.mousePressed(getSentiment);
  //når knappen trykkes hentes sentiment scoren via getSentiment funktionen
}

function getSentiment() {
  //der trækkes data fra tekstboksen
  //data fra tekstboksen hentes og definere const text
  const text = inputBox.value();

  //der udregnes en sentiment 
  //data sentiment scoren udregnes og definere const prediction
  const prediction = sentiment.predict(text);

  //sentiment tallet vises på html siden
  //vises ved at const prediction.score hentes 
  sentimentResult.html('Sentiment score: ' + prediction.score);
}

function modelReady() {
  //når modellen er klar hentes den
  //viser status at modellen er hentet
  statusEl.html('Model loaded');
}

function draw() {
  //styling af model input og output
  statusEl.position(displayWidth*0.8, displayHeight*0.65)
  sentimentResult.position(displayWidth*0.8, displayHeight*0.7);
  inputBox.position(displayWidth*0.44, displayHeight*0.663);
  submitBtn.position(displayWidth*0.64, displayHeight*0.663);
  
  submitBtn.style("background-color", "white");
  submitBtn.style("border", "0px");
  
  inputBox.style("border", "0px");


  
}
