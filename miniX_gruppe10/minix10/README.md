# MiniX10 - gruppe 10

_Link til vores [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix10/index.html)_

_Link til vores kode:_ 
- _[sketch.js](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix10/sketch.js)_
- _[style.css](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix10/style.css)_



Vi har valgt programmet “Sentiment” inde fra ML5.js. Det er et program, hvor koden vurderer graden af følelse der i et stykke tekst der indtastes. Programmet genkender ord ved hjælp af IMDB reviews til at bestemme sætningens sentiment. Der anvendes anmeldelser med maks 200 ord og det er kun de 20.000 mest anvendte ord programmet genkender, og derfor kan sammenligne med, og vurderer ens følelser. 
Programmet bedømmer tekstens indhold af sentiment ud fra en skala, der starter fra 0, som repræsenterer negativt indhold i sætningen, og op til 1, der repræsenterer positivt indhold i sætningen. 

## Forklaring af koden
Selve koden vi kan se består ikke af meget. Der er to filer som udgøre programmet. En index fil samt et script. I index.html bliver der i `<head>` sektionen indlæst tre `<scripts>` hvor en del af koden/modellen ligger. Derefter kommer `<body>` sektionen hvor i der er placeret en overskrift i form af `<h1>`, paragraffer og `script.js` som køre sentiment modellen og udregner samt viser sentiment scoren. 

![ALT](ml5_original.png)

Som der kan ses på billedet har vi for hver linje skrevet kommentarer som forkalring på hvad de enkelte syntaxter gør. Som vi reflektere længere  nede i vores ReadMe kan det diskuteres om vi har opfattet vores kode korrekt da vi ikke har adgang til at ændre samt ikke er i stand til at forstå de scripts der andvendes. 

Der defineres globale værdier som senere hen anvendes til at udregne en sentiment score. I `function setup()` startes der ud med at defineres at der ikke anvendes et canvas. Det betyder at det som vi ser i programmet foregår i et html miljø hvilket. Opsætningen af dette kan ses i linje 16-20. Her dannes variablerne for `statusEl` der viser om modellen loades/er loadet, `inputBox` hvor det er muligt at indtaste sin sætning samt en styling af dette, dannelse af en submit knap ved `submitBtn` og til sidst selve sentiment scoren ved `sentimentResult`.

Derefter anvendes der `mousePressed()` hvor der defineres at når knappen trykkes skal det trækkes/udregnes en sentiment score.
![ALT](ml5_original2.png)

I `function getSentiment()` er hvor modellen trækker på den læring den har. I linje 30 trækkes der data fra tekstboksen og værdien/data fra dette input indsættes ved `const text`. Derefter udregnes sentiment scoren og der oprettes en `const prediction`. Til slut i funktionen vises sentiment scoren i html miljøet hvilket gøre trække data fra ´prediction`. 

I bunden af koden er der anvendt `function modelReady` som køres når modellen er klar til at blive hentet.

![ALT](ml5_original1.png)

## Vores ændringer
Vi har i den originale kode foretaget nogle ændringer, for at lave lidt om i programmet. Vi har lavet programmet om på den måde, at når man kører programmet skal det forestille at være Instagram man er inde på. Tekstfeltet derinde forestiller kommentarboksen fra Instagram. Når man så vælger at skrive noget i tekstfeltet, skal de forestille det scenarie inde på Instagram når man vælger at skrive en kommentar. Efterfølgende måler modellen hvorvidt ens kommentar er negativ eller positiv, ved at give en et decimaltal imellem 0 og 1. 0 betyder at den er negativ og 1 betyder at den er positiv. 

![ALT](Sentimentscore_screenshot.png)

Vi følte dette var et scenarie de fleste af os vil kunne sætte os ind i eftersom de fleste af os kender/benytter Instagram. Vi føler også personligt dette vil være en god egenskab for Instagram at have. Selvfølgelig opstår der her konflikter omrking hvilke forskellige ting folk opfatter som værende negative eller positive. Dog synes vi dette var en spændende måde at ændre i koden for at gøre den mere relevant og brugbar i vores nuværende samfund.

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix10/Sentimen_score.mov)

En del af ændringer vi har foretaget os er frontend baseret. Da det ikke har været muligt at foretage større ændrigner i backend og egentlig ændre programmets basic output forsøgte vi derffor at ændre forholdet samt brugen af programmets output.

I vores frontend ændringer har i forhold til syntaxer implementeret `function draw()`. I billedet forneden kan der ses hvordan vi har stylet modellen input og output. Derudover tilføjede vi også styling af paragrafferne i html miljøet ved at oprette en [css fil](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX_gruppe10/minix10/style.css). Her har vi blot skrevet nogle få linjer. 

![ALT](ml5_screenshot2.png)

## Diskussion og refleksion 
Vi har haft svært ved at ændre i koden, fordi der er tre scripts som vi ikke har kunnet få adgang til. På samme måde så har modellen programmet gør brug været i det vi kalder en black box. Vi har ikke haft muligheden for at se koden bag modellen, for at kunne analysere og forstå den, og for at kunne se hvordan den bearbejder den data den for givet. 

Grundet vores store uvished, har vi gjort vores bedste for at forstå de tre forskellige scripts. Dog uden mulighed for at lave store ændringer i koden. Vi mener at dette ofte kan ske, fordi hvornår forstår man egentligt et program? Vi forstår ikke maskinekoden inde i vores computere, men vi antager vi forstår hvordan den fungerer, og vi kan også betjene den. Dette er også et eksempel på en black box situation. Der indsættes et input som vi forstår, der skabes et output som vi forstår, men i virkeligheden er vi uvidende omkring hvordan computeren i bund og grund processerer inputtet og giver os vores ønskede output. 

Vi mener, man kan til en vis grad perspektivere til tegnsprog, en kommunikation alle kender, men kun nogen forstår.  Vi ved at det foregår med hænderne, og noget med rytmen, hvordan det ser ud, og at det kommunikere noget til en anden. Men vi ved ikke hvordan vedkommende forstår det, ser det i deres hoved eller  konkretiserer det. Fordi de aldrig har kommunikeret lige som os.
Black box situationen kan vi ofte stå i. Det er helt den samme situation vi har stået i med movie-review modellen der køres ‘’bag’’ selve programmet. Vi indsætter et input, modellen tages i brug og derefter får vi vores output, der i vores tilfælde er en måling af mængden af sentiment i vores sætning.
