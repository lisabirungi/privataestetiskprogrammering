# MiniX2: RunMe og ReadMe

<!-- blank line -->

_Link til [MiniX2](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX2/index.html)_

_Link til [Min kode](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX2/sketch.js)_

Lykkeds det med at besvare øvelsen på en fyldesgørende, interessant eller unik måde?
Denne MiniX skulle omhandle geometriske emojis. Jeg har i opgave visuelt lavet en emoji som består af forksellige geometriske former. Jeg anvender ```rect()```, ```arc()``` og ```circle()``` til at udforme en emoji. Emojien er simpel og har et glad udtryk. Under emojien er der placeret en knap som opfordre brugeren til at man trykker på den med teksten "Forstør mig!". 

![ALT](miniX2_screenshot.png)

Ved at trykke på knappen kan man se at emojien forstørres. Knappen fungere således at når man holder den nede vokser emojien og når knappen slippes stopper den. Forneden du via en GIF se hvordan emojien visuelt ser ud samt hvad der sker når der trykkes på knappen.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX2/MiniX2_GIF.gif)

<!-- blank line -->
----
<!-- blank line -->

Grundet årsager havde jeg ikke mulighed for at deltage i den uge hvor geometriske former og derfor føler jeg ikke denne MiniX er særlig god. Jeg har både brugt den lærte litteratur men også anvendt referencelisten fra [p5js.org](https://p5js.org). 

Min første mål var at kode emojien således at når knappen blev trykket ville emojien vibrere/ryste. Forsøgte at anvende ```random()``` på den måde at den ville få en tilfældig ny postion hvert sekund når knappen blev aktiviret og derved vibrere/ryste. 

DA jeg ikek kunne få det til at lykkedes forsøgte jeg mig derfter at få emojien til at vokse. Det er delvis lykkedes men havde problemer med at få de geometriske former til at vokse mere paralelt med hinanden. På nuværende tidpunkt forstørres emojien men den frovrænges imens det sker. 

<!-- blank line -->
----
<!-- blank line -->

I forhold til den kulturelle kontekst og hvordan det store udbred af forskellige emojis vi har i af emojis, har jeg med denne emoji forsøgt at gøre den så neutral som muligt.  Så vi i teorien kun har behov for en emoji.

I forhold til den kulturelle kontekst og hvordan emojien kulturelt kan anses, har jeg forsøgt at holde mig neutralt. Man kan mene at den gule standardfarve der for emojis ikke er 100% neutralt. Dog er det den farve størstedelen kender til når det kommer til emojis og anses af mange som neutral. Jeg har ikke forsøgt at implementere nogen former for kønsidentificerende elementer eller nogen kulturelle elementer. Har så vidt muligt blot forsøgt at skabe hvad man muligvis ville kalde for en "basic" emoji. 

Dog i mit forsøg i at gøre den neutral kan det diskuteres at jeg måske er kommet til at gøre den mere "standard" agtig. Med dette mener jeg at jeg ubevidst har forsøgt at lave en neutral emoji der ikke skulle støde nogle former for sociale og kulturelle grupper ved at lave "den original emoji". Men ved at reflektere over min emojis udseende kan jeg nu og forstå at den originale emoji som vi alle kender sikkert også blevet designet dengang ud fra en vis holdning og mening i forhold til hvem de dengang gerne ville ramme i forbindelse med deres emoji. 

Kan se fordelen ved at lave flere forskellige emojis der skaber diversitet i det udvalg man har. Af personlig erfaring anvender jeg oftest emojis der ligner mig angående hår og hudfarve. Jeg tror ikke det ændrede min verden da jeg kunne anvende emojis med brun hud, brunt hår og brune øjne. Men jeg kan forstå hvor skadeligt og stødende det må være hvis man konstant ser emojis der repræsenterer ens kultur forkert eller stiller det i et dårligt lys.  Jeg synes at den voksende gruppe af emojis er en positiv ting, men kan også forestille mig at dem der står bag det også skal være opmærksomme på hvilke emojis de så inddrager. 

### Referenceliste
- [p5.js.org - circle( )](https://p5js.org/reference/#/p5/circle)
- [p5.js.org - square( )](https://p5js.org/reference/#/p5/arc)
- [p5.js.org - rect( )](https://p5js.org/reference/#/p5/rect)
- [p5.js.org - mouseClicked( )](https://p5js.org/reference/#/p5.Element/mouseClicked)
- [p5.js.org - color( )](https://p5js.org/reference/#/p5/color)
- [p5.js.org - fill( )](https://p5js.org/reference/#/p5/fill)
- [p5.js.org - create.Button( )](https://p5js.org/reference/#/p5/createButton)
- [gitlab.com - README guide](https://about.gitlab.com/handbook/markdown-guide/)
- [github.com - README guide](https://docs.github.com/en)
