
//variabler for spacesound baggrundsmusik
let spaceSound;
let button;
let pressed;

//variabel for anvendt tekst font
let spaceFont;

//variabler for starposition
var deg = 0;
var deg1 = 0;
var deg2 = 0;
var deg3 = 0;
var deg4 = 0;
var deg5 = 0;
var deg6 = 0;
var deg7 = 0;
var deg8 = 0;
var deg9 = 0;
var stars;

function preload() {
  spaceFont = loadFont("FastHand.ttf");
  spaceSound = loadSound("stasis_by_finval.mp3");
}

function setup() { 
  createCanvas (displayWidth, displayHeight);

  //stjerner
  //createGrapics gør mine stjerne til et objekt - det opjekt skal vises hvilket sker gennem imgae(stars) nede draw
  stars = createGraphics (displayWidth, displayHeight);
    //der sættes en grænse for hvor mange gange den skal loope - hvor mange stjerne der skal laves - 1000 stjerner
    for (let i = 0; i < 700; i++) {
      stars.fill(255,random(250));
      stars.noStroke();
      stars.ellipse(random(width), random(height), random(2, 5));
    }

  //oprettelse af knap
  // button.style("font-family", "FastHand.ttf");
  // button.style("font-size", "30px");
  button = createButton("Take off!");
  button.position(115, 535);
  button.size(120, 60);
  button.mousePressed(playSound);
} 

function playSound() {
  spaceSound.loop();
}
  

function draw() { 
  background(24, 32, 48);

//stjernener bliver til et grafisk element - bliver til et image
  image(stars, 0, 0);

//info tekstboks
  fill(80,81,80, 100)
  noStroke();
  rect(65, 95, 225, 540, 15);
  textFont(spaceFont);
  fill(255);
  textSize(15);
  text("Skrue op for din lyd", 90, 475);
  text("& tryk paa knappen", 90, 500);
  textSize(15);

  textFont(spaceFont);
  textSize(20);
  fill(255);
  text("Solsystemet", 90, 135);
  textSize(15);

  fill(255);
  text("Solen", 135, 170);
  stroke(254, 192, 56, 200);
  strokeWeight(2)
  fill(254, 155, 64, 200);
  ellipse (110, 165, 15, 15);

  fill(255);
  noStroke();
  text("Merkur", 135, 200);
  fill(105, 105, 105, 200);
  ellipse (110, 195, 15, 15);

  fill(255);
  noStroke();
  text("Venus", 135, 230);
  fill(225, 211, 199, 200);
  ellipse (110, 225, 15, 15);

  fill(255);
  noStroke();
  text("Jorden", 135, 260);
  fill(109, 203, 231, 200);
  ellipse (110, 255, 15, 15);

  fill(255);
  noStroke();
  text("Mars", 135, 290);
  fill(236, 65, 36, 200);
  ellipse (110, 285, 15, 15);

  fill(255);
  noStroke();
  text("Jupiter", 135, 320);
  fill(211, 185, 169, 200);
  ellipse (110, 315, 15, 15);

  fill(255);
  noStroke();
  text("Saturn", 135, 350);
  fill(214, 195, 128, 200);
  ellipse (110, 345, 15, 15);

  fill(255);
  noStroke();
  text("Uranus", 135, 380);
  fill(154,228,230, 200);
  ellipse (110, 375, 15, 15);

  fill(255);
  noStroke();
  text("Neptun", 135, 410);
  fill(67, 114, 239, 200);
  ellipse (110, 405, 15, 15);

  fill(255);
  noStroke();
  text("Pluto", 135, 440);
  fill(208, 216, 222, 200);
  ellipse (110, 435, 15, 15);

  

//bane for planeternes kredsløb
  noFill();
  stroke(255, 255, 255, 30);
  circle(width*0.55, height/2, 105);
  circle(width*0.55, height/2, 156);
  circle(width*0.55, height/2, 235);
  circle(width*0.55, height/2, 325);
  circle(width*0.55, height/2, 440);
  circle(width*0.55, height/2, 565);
  circle(width*0.55, height/2, 670);
  circle(width*0.55, height/2, 755);
  circle(width*0.55, height/2, 810);
  

//Sol
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg1));
    fill(254, 155, 64, 200);
    stroke(254, 192, 56, 200);
    strokeWeight(5)
    ellipse (0, 0,80,80);
  pop();


//Merkur
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg3));
    fill(105, 105, 105, 200);
    noStroke();
    ellipse (37, 37, 20, 20);
  pop(); 

//Venus
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg2));
    fill(225, 211, 199, 200);
    noStroke();
    ellipse (55, 55, 30, 30);
  pop(); 

//Jorden
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg5));
    fill(109, 203, 231, 200);
    noStroke();
    ellipse (83, 83, 50, 50);
  pop(); 

//Mars
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg4));
    fill(236, 65, 36, 200);
    noStroke();
    ellipse (115, 115, 40, 40);
  pop(); 

//Jupiter
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg1));
    fill(211, 185, 169, 200);
    noStroke();
    ellipse (154, 154, 70, 70);
  pop(); 

//Saturn
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg8));
    fill(214, 195, 128, 200);
    noStroke();
    ellipse (200, 200, 60, 60);
  pop(); 

//Uranus
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg7));
    fill(154,228,230, 200);
    noStroke();
    ellipse (237, 237, 45, 45);
  pop(); 

//Neptun
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg6));
    fill(67, 114, 239, 200);
    noStroke();
    ellipse (267, 267, 40, 40);
  pop(); 

//Pluto
  push(); 
    translate (width*0.55, height/2);
    rotate (radians (deg));
    fill(208, 216, 222, 200);
    noStroke();
    ellipse (287, 287, 15, 15);
  pop(); 

//Kredsløbshastighed - hver planet har fået den kredsløbshastighed der passer til hvor hurtige de rotere rund om jorden
//deg0 er den langsomeste planet og deg9 er den  hurtigste
  deg +=0.01;
  deg1 +=0.1;
  deg2 +=0.2;
  deg3 +=0.3;
  deg4 +=0.4;
  deg5 +=0.5;
  deg6 +=0.6;
  deg7 +=0.7;
  deg8 +=0.8;
  deg9 +=0.9;
  

}
