# MiniX1: RunMe og ReadMe

_Link til [MiniX3](https://lisabirungi.gitlab.io/privataestetiskprogrammering/MiniX3/index.html)_

_Link til [Min kode](https://lisabirungi.gitlab.io/privataestetiskprogrammering/MiniX3/sketch1.js)_

**What do you want to explore and/or express?**
<br>
<br>
I denne MiniX3 har jeg valgte at visualisere solsystemet ved brug af throbber. Jeg fandt solsystemet og planeternes rotation omkring solen, som en god måde at visualisere en hvordan en throbber fungere. Jeg ville også gerne forsøge at lege med forskellige hastigheder af planeternes rotation.  Ved at indstille throbberen (planeterne) til at have forskellige hastigheder vil det forstærke det visuelle formål at få programmet til at ligne et solsystem. 

![ALT](miniX3_screenshot.png)
<!-- blank line -->
----
<!-- blank line -->

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**
<br>
<br>
Alle planeterne bevæger sig i forskellige hastigheder. Hver planet har fået sin egen hastighed. Jeg undersøgte hvor hurtig planeterne var i forhold til hinanden, og jeg har derefter givet den langsomste planet den laveste rotationshastighed, og den hurtigste planet har fået den hurtigste. Resten har fået en hastighed der passer til hvor hurtig de er i forhold til de andre. For at bestemme planeternes rotationshastighed, har jeg defineret de 9 forskellige hastigheder. I syntaxen kan der ses at jeg definerer 9 variabler `deg()`, `deg1()`, `deg2()` osv. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/MiniX3/Billede1.png)
 
Derefter gav jeg hver variabel sine hastighed deg +=0.01. I billedet kan man se at hastigheden stiger ved hver variabel. Deg er den langsomste hastighed er deg og den hurtigste er `deg9()`. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/MiniX3/Billede2.png)
 
Da Pluto er den langsomste planet i solsystemet defineres `deg9` i `rotation`.
 
![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/MiniX3/Billede3.png)
<!-- blank line -->
----
<!-- blank line -->

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**
<br>
<br>
Jeg har ikke tidligere tænkt over throbber og hvor jeg har oplevet dem før da begrebet throbber ikke var et begreb jeg var bekendt med. Men nu hvor jeg er blevet bevidst om begrebet, er jeg nu bevidst om hvor mange steder jeg oplever det. Når jeg streamer film, musik, skal loade en side eller der er en ”ventetid” på noget i forbindelse med noget digitalt. Det er en sjov tanke over hvor meget tid jeg egentlig har brugt på throbber gennem mit liv. Tid jeg har givet på at vente. 

Jeg opfatter en throbber, eller et loading symbol, som en tydelig indikation på at der sker noget i baggrunden, men at det tager tid og jeg skal vente. Somme tider kan det være frustrerende, for oftest vises der ikke hvad der sker i baggrunden, og hvor lang tid mere jeg skal vente. Man kan overføre selve throbberen til andre elementer i ens liv. Den fysiske udgave af en throbber kan for eksempel være at trille tommelfingre. Det er ligesom throbberen et øjeblik hvor der ventes og hvor tiden skal gå.

Hvis en throbber, eller loading symbol, skal ændres tror jeg at det ville være en fordel, hvis der blev oplyst hvad der sker i baggrunden samt hvad der kommer til at ske efterfølgende. Tror især at frustrationen i forbindelse med en throbber er at man ikke kan se hvad der sker udover et lille symbol der på en måde kræver af en at mans skal vente uanset hvad. Nu hvor vi er blevet mere digitaliseret, vi finder nye og hurtigere løsninger på hverdags ting hvilket gør at vi burde have mere tid men sjovt nok er vi blevet mere utålmodige som om vi har mindre tid at gøre godt med. 
