# MiniX4: Data Capture

_Link til min [RunMe](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/index.html)_

_Link til mit [kode](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/sketch.js)_

Jeg havde lidt svært ved at finde en vinkel, på det med at se på data capture med kritiske øjne. I sidste ende endte jeg med hvad jeg selv ville beskrive, som en visuelle repræsentation af et overvågningsrum. Min RunMe viser en blå skærm, hvor jeg visuelt har kodet hvad ligner to højtaler og et tv. 

Nedenunder er der et kontrolpanel. Kontrolpanelet har ikke nogen funktion men er blot med for at forstærke følelsens af, at det ser ud som om man overvåger nogen. Forneden kan der ses et billede af hvordan programmet ser ud i begyndelsen.

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/screenshot_start.bmp)


I venstre side er der placeret en tekst, der informere at ved at trykke på tasterne S og T, kan man aktivere to forskellige funktioner. Ved at trykke på S tasten på sit tastatur, er det som om man ”slukker for lyset" i overvågningsrummet. Baggrunden bliver mørk og i venstre side af skærmen vises der information om datasikkerheden i Danmark. Ved at trykke på T tasten ”tændes lyset igen” og baggrunden bliver blå igen, og et nyt stykke fakta om overvågning i Danmark vises i venstre side af skærmen. 

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/screenshot_S.bmp)

![ALT](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/screenshot_T.bmp)

Tv’et anvender ens computers webcam, så det giver fornemmelse af at det er brugeren der overvåges. Højtalerne på hver side af tv’et burde anvende computeren mikrofon. Speakeren burde bevæge sig alt efter hvilken lydstyrke der opfanges. Uheldigvis kunne jeg ikke få det til at virke over Gitlab og kunne kun få det til at fungere lokalt. Neden under i videoen kan der dog ses hvordan det ville se ud.

![](https://lisabirungi.gitlab.io/privataestetiskprogrammering/miniX4/MiniX4.mov)

<!-- blank line -->
----
<!-- blank line -->

Som tidligere nævnt havde jeg lidt svært ved at komme på en ide i forhold til hvordan jeg bedst mulig kunne visualisere emnet ”capture all” og samtidig visualiserer det kritisk. Jeg har forsøgt at opstille det således, at det ligner at man befinder sig i et overvågningsrum, som netop gør brug af indsamling af data for, at man kan holde øje med folk. Jeg har forsøgt at inddrage både brugen af audio data, video data og keyboard data. 

<!-- blank line -->
----
<!-- blank line -->

Jeg tror vi lever i en tid nu hvor det er meget og nærmest umuligt ikke at udlevere data uanset om man vil det eller ej. Især i forbindelse med brugen af digitale genstande og platforme. En forstyrrende tanker jeg kan have når det kommer til data capture er når jeg gør brug af min computer, telefon, befinder mig online osv. hvordan kan jeg være 100% sikker hvad det er de indsamler om mig.

Vi lever i en særdeles digitalisere verden og her i Danmark bruges data om os til utrolig meget og det gør at det jo netop bliver så værdifuldt. Og derved er det derfor også så alvorligt, når der sker et databrud eller når data bliver misbrugt. 

Tror at ved data netop er blevet så værdifuldt for os, er de forskellige metoder inden for data capture vigtige for hvordan vi kan fungere i hverdagen og hvordan mange dele af vores samfund kan fungere. Og jo mere avanceret metoderne og mulighederne inden for data capture bliver, tror jeg også at vi som samfund og kulturelt udvikler os sammen med det. Både i en dårlige og god retning. 


## Referencer

- p5.js Referencer - https://p5js.org/reference/
- Fakta om overvågning i DK - https://faktalink.dk/overvagning-samfundet 
- Fakta om datasikkerhed i DK - https://www.datatilsynet.dk/sikkerhedsbrud/anmeld-sikkerhedsbrud
- Billede brug til kontrolpanel - https://www.freepik.com/free-vector/switches-buttons-control-panel-vector-illustrations-set-retro-control-console-terminal-elements-dials-knobs-dashboard-system-monitor-display-technology-equipment-concept_28480839.htm#query=control%20board&position=0&from_view=search&track=ais 

